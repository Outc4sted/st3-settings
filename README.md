st3-settings
============

Running `link.sh` adds the ST3 PPA and installs if there's no current ST3 installation.
Then it creates symlinks for ST3 session buffers and preferences creating a unified workspace
that's instantly synced with any changes and quick to get up to full speed on other computers.

### Packages installed:

* [Package Control](https://sublime.wbond.net/packages/Package%20Control)
* [Theme - Flatland](https://sublime.wbond.net/packages/Theme%20-%20Flatland)
* [SublimeLinter](https://sublime.wbond.net/packages/SublimeLinter)
* [Gitignore](https://sublime.wbond.net/packages/Gitignore)
* [BracketHighlighter](https://sublime.wbond.net/packages/BracketHighlighter)
* [SublimeCodeIntel](https://sublime.wbond.net/packages/SublimeCodeIntel)
* [Alignment](https://sublime.wbond.net/packages/Alignment)
* [Jade](https://sublime.wbond.net/packages/Jade)
* [Better CoffeeScript](https://sublime.wbond.net/packages/Better%20CoffeeScript)
* [Jasmine CoffeeScript](https://sublime.wbond.net/packages/Jasmine%20CoffeeScript)
* [AngularJS CoffeeScript](https://github.com/Iristyle/Sublime-AngularJS-Coffee-Completions)

--------------------------------

### Installing

1. Clone into instant backup folder like dropbox or gdrive
    ```
    cd ~/Dropbox/workspace
    git clone git@github.com:Outc4sted/st3-settings.git
    cd st3-settings/
    ```

2. In `link.sh`, edit the line containing `st3CloudSettings` to point to where you're storing the settings folders

3. Run `sudo sh links.sh`

```
                                              ,_    /) (\    _,
                                               >>  <<,_,>>  <<
                                              //   _0.-.0_   \\
                                              \'._/       \_.'/
                                               '-.\.--.--./.-'
                                               __/ : :Y: : \ _
                                       ';,  .-(_| : : | : : |_)-.  ,:'
                                         \\/.'  |: : :|: : :|  `.\//
                                          (/    |: : :|: : :|    \)
                                                |: : :|: : :;
                                               /\ : : | : : /\
                                              (_/'.: :.: :.'\_)
                                               \\  `""`""`  //
                                                \\         //
                                                 ':.     .:'
```
