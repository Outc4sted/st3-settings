#!/bin/bash

# This is where you're real settings are
st3CloudSettings=~/Dropbox/workspace/settings/st3-settings

###########################################################

# This is where ST3 stores config files by default
st3DefaultSettings=~/.config/sublime-text-3

# Set up path variables
st3LocalFolder=$st3CloudSettings/Local
st3UserFolder=$st3CloudSettings/User
st3DefaultPackages=$st3DefaultSettings/Packages

# Validate sublime-text package installation
sudo dpkg -l | grep -qw sublime-text || (add-apt-repository ppa:webupd8team/sublime-text-3 -y && apt-get update && apt-get install sublime-text-installer -y)

# download package controller and create initial directories if they aren't already there
wget -P ~/.config/sublime-text-3/Installed\ Packages https://sublime.wbond.net/Package%20Control.sublime-package
mkdir ~/.config/sublime-text-3/Packages

# package manager might need these
sudo apt-get install libsslcommon2 curl -y

# symlink user preferences folder
cd $st3DefaultPackages
rm -f User
ln -s $st3UserFolder

# symlink st3 session folder
cd $st3DefaultSettings
rm -f Local
ln -s $st3LocalFolder

exit
